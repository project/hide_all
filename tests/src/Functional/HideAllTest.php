<?php

namespace Drupal\Tests\hide_all\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Simple test to ensure that main page loads with module enabled.
 *
 * @group hide_all
 */
class HideAllTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['hide_all'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests that the modules list page with hide_all module.
   */
  public function testModulesListPage() {
    // By default, even though the user has the access, but denied.
    $user = $this->drupalCreateUser(['administer modules']);
    $this->drupalLogin($user);
    $this->drupalGet(Url::fromRoute('system.modules_list'));
    $this->assertSession()->statusCodeEquals(403);

    // Remove the under testing route name from the settings.
    $route_names = $this->config('hide_all.settings')->get('route_names');
    $this->config('hide_all.settings')->set('route_names', array_filter($route_names, function ($item) {
      return !($item === 'system.modules_list');
    }))->save();
    $this->rebuildAll();
    $this->drupalGet(Url::fromRoute('system.modules_list'));
    $this->assertSession()->statusCodeEquals(200);

    // Revert the default settings.
    $this->config('hide_all.settings')->set('route_names', $route_names)->save();
    $this->rebuildAll();
    $this->drupalGet(Url::fromRoute('system.modules_list'));
    $this->assertSession()->statusCodeEquals(403);

    // Disable hide all.
    $this->config('hide_all.settings')->set('status', FALSE)->save();
    $this->rebuildAll();
    $this->drupalGet(Url::fromRoute('system.modules_list'));
    $this->assertSession()->statusCodeEquals(200);

  }

}
