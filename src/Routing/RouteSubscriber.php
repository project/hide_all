<?php

namespace Drupal\hide_all\Routing;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * RouteSubscriber constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $settings = $this->configFactory->get('hide_all.settings');
    // If it's disabled, do nothing.
    if (!$settings->get('status')) {
      return;
    }
    $route_names = $settings->get('route_names');
    if (!empty($route_names) && is_array($route_names)) {
      foreach ($collection->all() as $name => $route) {
        if (in_array($name, $route_names, TRUE)) {
          $route->setRequirement('_access', 'FALSE');
        }
      }
    }
  }

}
